﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace prjLab02_01
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }
        //Esto permitirá mover el formulario :D
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        //Funcion para ingresar al sistema
        private void btnIngresar_Click(object sender, EventArgs e)
        {
            string[] users = { "User", "Frenrihr", "Carlos", "Admin" };
            string[] passwords = { "password", "frenrihr", "Quispe", "admin" };
            string userText = txtUsername.Text;
            string passText = txtContraseña.Text;
            if (users.Contains(userText))
            {
                int index = Array.IndexOf(users, userText);
                if(passwords[index]==passText)
                {
                    PrincipalMDI principalMDI = new PrincipalMDI();
                    principalMDI.Show();
                    Hide();
                } else
                {
                    MessageBox.Show("Crendenciales incorrecta");
                }
            } else
            {
                MessageBox.Show("Crendenciales incorrecta");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void titleBar_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(Handle, 0x112, 0xf012, 0);
        }

        private void linkTwitter_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Specify that the link was visited.
            this.linkTwitter.LinkVisited = true;
            // Navigate to a URL.
            System.Diagnostics.Process.Start("https://twitter.com/frenrihr_code");
        }

        private void btnIngresar_Paint(object sender, PaintEventArgs e)
        {
            //Rounded rectangle corder radius. The radius must be less than 10.
            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();// Create a line
            Rectangle myRectangle = btnIngresar.ClientRectangle;
            myRectangle.Inflate(0, 20);
            path.AddEllipse(myRectangle);
            btnIngresar.Region = new Region(path);
        }
    }
}
