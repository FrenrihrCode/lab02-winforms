﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prjLab02_01
{
    public partial class PrincipalMDI : Form
    {
        public PrincipalMDI()
        {
            InitializeComponent();
        }
        //Funciones para abrir las demás ventanas.
        private void mnuManUsuarios_Click(object sender, EventArgs e)
        {
            manUsuario frm = new manUsuario();
            frm.MdiParent = this;
            frm.Show();
        }

        private void mnuManProductos_Click(object sender, EventArgs e)
        {
            manProductos frm = new manProductos();
            frm.MdiParent = this;
            frm.Show();
        }

        private void mnuManCategorias_Click(object sender, EventArgs e)
        {
            manCategorias frm = new manCategorias
            {
                MdiParent = this
            };
            frm.Show();
        }

        private void mnuProRegVenta_Click(object sender, EventArgs e)
        {
            regVentas frm = new regVentas
            {
                MdiParent = this
            };
            frm.Show();
        }

        private void mnuSisSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //Hacer un efecto de cierre de sesión.
        private void mnuSisCerrarSesion_Click(object sender, EventArgs e)
        {
            frmLogin principalMDI = new frmLogin();
            principalMDI.Show();
            Close();
        }

        private void mnuRepUsuarios_Click(object sender, EventArgs e)
        {
            repUsuarios frm = new repUsuarios
            {
                MdiParent = this
            };
            frm.Show();
        }

        private void mnuRepClientes_Click(object sender, EventArgs e)
        {
            repClientes frm = new repClientes
            {
                MdiParent = this
            };
            frm.Show();
        }
    }
}
